const sayHello = require('../src/hello');

test(`string returning Hello I'm Jest`, () => {
    expect(sayHello()).toMatch(`Hello I'm Jest`);
})