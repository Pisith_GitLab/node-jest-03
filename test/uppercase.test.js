const uppercase = require('../src/uppercase');

test(`test uppercase test to TEST`, () => {
    return uppercase('test').then(str => {
        expect(str).toMatch('TEST');
    })
})

test(`test uppercase of '' and return Empty String`, () => {
    return uppercase('').catch(e => {
        expect(e).toMatch('Empty String');
    })
})

// Using async await
test(`test uppercase of 'test' using async await`, async () => {
    expect.assertions(1);
    const str = await uppercase('test');
    expect(str).toMatch('TEST');
})

test(`test uppercase of '' and return Empty String using Async Await`, async () => {
    expect.assertions(1);
    try {
        await uppercase('');
    } catch (err) {
        expect(err).toMatch('Empty String');
    }
})
